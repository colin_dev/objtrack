// ObjTrackTest.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
// Windows
#include <windows.h>
#include <vector>
#include <iostream>

// DX
#include "inc/DxCommon.h"

// shader
#include "VertexShader.h"
#include "PixelShader.h"

// Resource
#include "Resource.h"

// Objtrack
#include "ObjTrackInterface.h"

using namespace std;

// Format constants
const UINT32 WINDOW_WIDTH = 1280;
const UINT32 WINDOW_HEIGHT = 960;
const UINT32 FPS = 60;

typedef struct _THREAD_DATA
{
    HANDLE hThread;
    DWORD tId;
    HANDLE evExit;
    HANDLE evTimer;
}THREAD_DATA, *PTHREAD_DATA;

//--------------------------------------------------------------------------------------
// Global Variables
//--------------------------------------------------------------------------------------
HINSTANCE                           g_hInst = NULL;
HWND                                g_hMainWnd = NULL;
THREAD_DATA                         g_tData = { 0 };

//--------------------------------------------------------------------------------------
// Forward declarations
//--------------------------------------------------------------------------------------
HRESULT InitWindow(HINSTANCE, int);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
DWORD WINAPI RenderThread(_In_ PVOID pParam);
VOID CALLBACK PerodicTimerCB(PVOID pParam, BOOLEAN bTimerOrWaitFired);

//--------------------------------------------------------------------------------------
// Entry point to the program. Initializes everything and goes into a message processing 
// loop. Idle time is used to render the scene.
//--------------------------------------------------------------------------------------
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    BOOL isSuccess = FALSE;

    if (FAILED(InitWindow(hInstance, nCmdShow)))
        return 0;

    g_tData.evExit = ::CreateEvent(NULL, TRUE, FALSE, NULL);
    g_tData.evTimer = ::CreateEvent(NULL, TRUE, FALSE, NULL);

    HANDLE hTimer = NULL;
    HANDLE hTimerQueue = NULL;
    hTimerQueue = CreateTimerQueue();
    if (hTimerQueue)
    {
        if (CreateTimerQueueTimer(&hTimer, hTimerQueue,
            (WAITORTIMERCALLBACK)PerodicTimerCB, &g_tData.evTimer, 1000 / FPS, 1000 / FPS, WT_EXECUTEDEFAULT))
        {
            isSuccess = TRUE;
        }
    }

    ::ResetEvent(g_tData.evExit);
    ::ResetEvent(g_tData.evTimer);
    g_tData.hThread = ::CreateThread(nullptr, 0, RenderThread, &g_tData, 0, &(g_tData.tId));

    // Main message loop
    MSG msg = { 0 };
    while (WM_QUIT != msg.message)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        ::Sleep(1000 / FPS);
    }

    if (isSuccess)
    {
        DeleteTimerQueueTimer(hTimerQueue, hTimer, NULL);
        DeleteTimerQueue(hTimerQueue);
    }
    ::SetEvent(g_tData.evExit);
    ::WaitForSingleObjectEx(g_tData.hThread, INFINITE, FALSE);
    SafeCloseHandle(g_tData.evExit);
    SafeCloseHandle(g_tData.evTimer);

    return (int)msg.wParam;
}

//--------------------------------------------------------------------------------------
// Register class and create window
//--------------------------------------------------------------------------------------
HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow)
{
    // Register class
    WNDCLASSEX wcex;
    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(hInstance, (LPCTSTR)IDI_APPICON);
    wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = L"ObjTrackTestWindowClass";
    wcex.hIconSm = LoadIcon(wcex.hInstance, (LPCTSTR)IDI_APPICON);
    if (!RegisterClassEx(&wcex))
        return E_FAIL;

    // Create window
    g_hInst = hInstance;
    RECT rc = { 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT };
    AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);
    g_hMainWnd = CreateWindow(L"ObjTrackTestWindowClass", L"ObjTrackTest", WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance,
        NULL);
    if (!g_hMainWnd)
    {
        return E_FAIL;
    }

    ShowWindow(g_hMainWnd, nCmdShow);
    UpdateWindow(g_hMainWnd);

    return S_OK;
}

//--------------------------------------------------------------------------------------
// Called every time the application receives a message
//--------------------------------------------------------------------------------------
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;

    switch (message)
    {
    case WM_PAINT:
        hdc = BeginPaint(hWnd, &ps);
        EndPaint(hWnd, &ps);
        break;

    case WM_DESTROY:
        PostQuitMessage(0);
        break;

    case WM_SIZE:
        break;

    case WM_MOVE:
        break;

    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }

    return 0;
}

DWORD WINAPI RenderThread(_In_ PVOID pParam)
{
    PTHREAD_DATA pTData = (PTHREAD_DATA)pParam;

    RECT rc;
    GetClientRect(g_hMainWnd, &rc);
    UINT width = rc.right - rc.left;
    UINT height = rc.bottom - rc.top;

    D3D11_VIEWPORT vp;
    vp.Width = (FLOAT)width;
    vp.Height = (FLOAT)height;
    vp.MinDepth = 0.0f;
    vp.MaxDepth = 1.0f;
    vp.TopLeftX = 0;
    vp.TopLeftY = 0;

    HRESULT hr = S_OK;
    D3D_DRIVER_TYPE driverType = D3D_DRIVER_TYPE_NULL;
    D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_11_1;
    ID3D11Device *pDx11Dev = NULL;
    ID3D11DeviceContext *pDx11DevCtx = NULL;
    IDXGISwapChain *pDxgiSwapChain = NULL;
    ID3D11Texture2D *pBackBuffer = NULL;
    ID3D11RenderTargetView *pRTV = NULL;
    D3D11_TEXTURE2D_DESC texDesc = { 0 };
    ID3D11Texture2D *pTex = NULL;
    ID3D11ShaderResourceView *pTexSRV = NULL;
    ID3D11Texture2D *pSharedTex = NULL;
    HANDLE hSharedTex = NULL;
    ID3D11VertexShader *pVS = NULL;
    ID3D11PixelShader *pPS = NULL;
    ID3D11InputLayout *pIL = NULL;
    ID3D11Buffer *pVB = NULL;
    ID3D11Buffer *pIB = NULL;
    ID3D11Buffer *pCBNeverChanges = NULL;
    ID3D11Buffer *pCBChangeOnResize = NULL;
    ID3D11Buffer *pCBChangesEveryFrame = NULL;
    ID3D11SamplerState *pSS = NULL;
    XMMATRIX mWorld;
    XMMATRIX mView;
    XMMATRIX mProj;

    UINT createDeviceFlags = 0;
#ifdef _DEBUG
    createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

    D3D_DRIVER_TYPE driverTypes[] =
    {
        D3D_DRIVER_TYPE_HARDWARE,
        D3D_DRIVER_TYPE_WARP,
        D3D_DRIVER_TYPE_REFERENCE,
    };
    UINT numDriverTypes = ARRAYSIZE(driverTypes);

    D3D_FEATURE_LEVEL featureLevels[] =
    {
        D3D_FEATURE_LEVEL_11_1,
        D3D_FEATURE_LEVEL_11_0,
    };
    UINT numFeatureLevels = ARRAYSIZE(featureLevels);

    DXGI_SWAP_CHAIN_DESC sd;
    ZeroMemory(&sd, sizeof(sd));
    sd.BufferCount = 1;
    sd.BufferDesc.Width = width;
    sd.BufferDesc.Height = height;
    sd.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;

    sd.BufferDesc.RefreshRate.Numerator = FPS;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.OutputWindow = g_hMainWnd;
    sd.SampleDesc.Count = 1;
    sd.SampleDesc.Quality = 0;
    sd.Windowed = TRUE;
    sd.SwapEffect = DXGI_SWAP_EFFECT_SEQUENTIAL;

    for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
    {
        driverType = driverTypes[driverTypeIndex];
        hr = D3D11CreateDeviceAndSwapChain(NULL, driverType, NULL, createDeviceFlags, featureLevels, numFeatureLevels,
            D3D11_SDK_VERSION, &sd, &pDxgiSwapChain, &pDx11Dev, &featureLevel, &pDx11DevCtx);
        if (SUCCEEDED(hr))
            break;
    }

    // Create a render target view
    if (SUCCEEDED(hr))
    {
        hr = pDxgiSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
    }

    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateRenderTargetView(pBackBuffer, NULL, &pRTV);
    }

    // Prepare VS
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateVertexShader(g_VS, ARRAYSIZE(g_VS), nullptr, &pVS);
    }

    // Define the input layout
    D3D11_INPUT_ELEMENT_DESC layout[] =
    {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    };
    UINT numElements = ARRAYSIZE(layout);

    // Create the input layout
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateInputLayout(layout, numElements, g_VS, ARRAYSIZE(g_VS), &pIL);
    }

    // Prepare PS
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreatePixelShader(g_PS, ARRAYSIZE(g_PS), nullptr, &pPS);
    }

    // Create VB
    VERTEX vertices[] =
    {
        { XMFLOAT3(-1.0f, -1.0f, 0.0f), XMFLOAT2(1.0f, 1.0f) },
        { XMFLOAT3(-1.0f, 1.0f, 0.0f), XMFLOAT2(1.0f, 0.0f) },
        { XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 0.0f) },
        { XMFLOAT3(1.0f, -1.0f, 0.0f), XMFLOAT2(0.0f, 1.0f) },
    };

    D3D11_BUFFER_DESC bd;
    ZeroMemory(&bd, sizeof(bd));
    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = sizeof(VERTEX) * 4;
    bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    bd.CPUAccessFlags = 0;
    D3D11_SUBRESOURCE_DATA InitData;
    ZeroMemory(&InitData, sizeof(InitData));
    InitData.pSysMem = vertices;
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateBuffer(&bd, &InitData, &pVB);
    }

    // Set vertex buffer
    UINT stride = sizeof(VERTEX);
    UINT offset = 0;

    // Create IB
    WORD indices[] =
    {
        0,1,3,
        3,1,2,
    };

    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = sizeof(WORD) * 6;
    bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
    bd.CPUAccessFlags = 0;
    InitData.pSysMem = indices;
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateBuffer(&bd, &InitData, &pIB);
    }

    // Create the constant buffers
    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = sizeof(CBNeverChanges);
    bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    bd.CPUAccessFlags = 0;
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateBuffer(&bd, NULL, &pCBNeverChanges);
    }

    bd.ByteWidth = sizeof(CBChangeOnResize);
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateBuffer(&bd, NULL, &pCBChangeOnResize);
    }

    bd.ByteWidth = sizeof(CBChangesEveryFrame);
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateBuffer(&bd, NULL, &pCBChangesEveryFrame);
    }

    if (SUCCEEDED(hr))
    {
        pBackBuffer->GetDesc(&texDesc);
        texDesc.Usage = D3D11_USAGE_DEFAULT;
        texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
        texDesc.CPUAccessFlags = 0;
        texDesc.MiscFlags = 0;
        hr = pDx11Dev->CreateTexture2D(&texDesc, NULL, &pTex);
    }

    if (SUCCEEDED(hr))
    {
        D3D11_SHADER_RESOURCE_VIEW_DESC ShaderDesc;
        ShaderDesc.Format = texDesc.Format;
        ShaderDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
        ShaderDesc.Texture2D.MostDetailedMip = 0;
        ShaderDesc.Texture2D.MipLevels = -1;
        if (SUCCEEDED(hr))
        {
            hr = pDx11Dev->CreateShaderResourceView(pTex, &ShaderDesc, &pTexSRV);
        }
    }

    if (SUCCEEDED(hr))
    {
        pBackBuffer->GetDesc(&texDesc);
        texDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;// or DXGI_FORMAT_R8G8B8A8_UNORM
        texDesc.Usage = D3D11_USAGE_DEFAULT;
        texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
        texDesc.CPUAccessFlags = 0;
        texDesc.MiscFlags = D3D11_RESOURCE_MISC_SHARED;
        hr = pDx11Dev->CreateTexture2D(&texDesc, NULL, &pSharedTex);
    }

    IDXGIResource *pDXGIResource = nullptr;
    if (SUCCEEDED(hr))
    {
        hr = pSharedTex->QueryInterface(__uuidof(IDXGIResource), reinterpret_cast<void**>(&pDXGIResource));
    }
    if (SUCCEEDED(hr))
    {
        hr = pDXGIResource->GetSharedHandle(&hSharedTex);
    }
    SafeRelease(pDXGIResource);

    if (SUCCEEDED(hr))
    {
        hr = ObjTrackInterface::StartObjTrack(hSharedTex);
    }

    // Create the sample state
    D3D11_SAMPLER_DESC sampDesc;
    ZeroMemory(&sampDesc, sizeof(sampDesc));
    sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
    sampDesc.MinLOD = 0;
    sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateSamplerState(&sampDesc, &pSS);
    }

    // Initialize the world matrices
    mWorld = XMMatrixIdentity();
    CBChangesEveryFrame cb;
    cb.mWorld = XMMatrixTranspose(mWorld);
    pDx11DevCtx->UpdateSubresource(pCBChangesEveryFrame, 0, NULL, &cb, 0, 0);

    // Initialize the view matrix
    XMVECTOR Eye = XMVectorSet(0.0f, 0.0f, -3.0f, 0.0f);
    XMVECTOR At = XMVectorSet(0.0f, 0.0f, -1.0f, 0.0f);
    XMVECTOR Up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
    mView = XMMatrixLookAtLH(Eye, At, Up);
    CBNeverChanges cbNeverChanges;
    cbNeverChanges.mView = XMMatrixTranspose(mView);
    pDx11DevCtx->UpdateSubresource(pCBNeverChanges, 0, NULL, &cbNeverChanges, 0, 0);

    // Initialize the projection matrix
    mProj = XMMatrixPerspectiveFovLH(XM_PIDIV4, width / (FLOAT)height, 0.01f, 100.0f);

    CBChangeOnResize cbChangesOnResize;
    cbChangesOnResize.mProjection = XMMatrixTranspose(mProj);
    pDx11DevCtx->UpdateSubresource(pCBChangeOnResize, 0, NULL, &cbChangesOnResize, 0, 0);

    // Render loop
    while ((SUCCEEDED(hr)) && (::WaitForSingleObjectEx(pTData->evExit, 0, FALSE) == WAIT_TIMEOUT))
    {
        if (::WaitForSingleObjectEx(pTData->evTimer, 1000, FALSE) == WAIT_OBJECT_0)
        {
            if (::WaitForSingleObjectEx(pTData->evExit, 0, FALSE) == WAIT_OBJECT_0)
            {
                break;
            }
            ::ResetEvent(pTData->evTimer);

            // Render
            float ClearColor[4] = { 0.0f, 0.125f, 0.3f, 1.0f }; // red, green, blue, alpha
            pDx11DevCtx->ClearRenderTargetView(pRTV, ClearColor);
            pDx11DevCtx->OMSetRenderTargets(1, &pRTV, NULL);
            pDx11DevCtx->RSSetViewports(1, &vp);
            pDx11DevCtx->VSSetShader(pVS, NULL, 0);
            pDx11DevCtx->PSSetShader(pPS, NULL, 0);
            pDx11DevCtx->IASetInputLayout(pIL);
            pDx11DevCtx->IASetVertexBuffers(0, 1, &pVB, &stride, &offset);
            pDx11DevCtx->IASetIndexBuffer(pIB, DXGI_FORMAT_R16_UINT, 0);
            pDx11DevCtx->VSSetConstantBuffers(0, 1, &pCBNeverChanges);
            pDx11DevCtx->VSSetConstantBuffers(1, 1, &pCBChangeOnResize);
            pDx11DevCtx->VSSetConstantBuffers(2, 1, &pCBChangesEveryFrame);
            pDx11DevCtx->PSSetConstantBuffers(2, 1, &pCBChangesEveryFrame);
            pDx11DevCtx->CopyResource(pTex, pSharedTex);
            pDx11DevCtx->PSSetShaderResources(0, 1, &pTexSRV);
            pDx11DevCtx->PSSetSamplers(0, 1, &pSS);
            pDx11DevCtx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
            pDx11DevCtx->DrawIndexed(6, 0, 0);

            // Present
            pDxgiSwapChain->Present(1, 0);
        }
    }

    // Cleanup
    ObjTrackInterface::StopObjTrack();
    if (pDx11DevCtx) pDx11DevCtx->ClearState();
    SafeRelease(pSS);
    SafeRelease(pTexSRV);
    SafeRelease(pTex);
    SafeRelease(pCBNeverChanges);
    SafeRelease(pCBChangeOnResize);
    SafeRelease(pCBChangesEveryFrame);
    SafeRelease(pIB);
    SafeRelease(pVB);
    SafeRelease(pIL);
    SafeRelease(pPS);
    SafeRelease(pVS);
    SafeRelease(pRTV);
    SafeRelease(pBackBuffer);
    SafeRelease(pDxgiSwapChain);
    SafeRelease(pDx11DevCtx);
    SafeRelease(pDx11Dev);
    return 0;
}

VOID CALLBACK PerodicTimerCB(PVOID pParam, BOOLEAN bTimerOrWaitFired)
{
    HANDLE ev = *(PHANDLE)pParam;
    ::SetEvent(ev);
}
