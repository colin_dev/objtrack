#pragma once

#ifndef __DX_COMMON_H__
#define __DX_COMMON_H__

#include <d3d11.h>
#include "DirectXMath.h"

using namespace DirectX;

typedef struct _VERTEX
{
    XMFLOAT3 Pos;
    XMFLOAT2 Tex;
}VERTEX;

struct CBNeverChanges
{
    XMMATRIX mView;
};

struct CBChangeOnResize
{
    XMMATRIX mProjection;
};

struct CBChangesEveryFrame
{
    XMMATRIX mWorld;
    XMFLOAT4 vMeshColor;
};

inline void SafeCloseHandle(HANDLE& h)
{
    if (h != nullptr)
    {
        CloseHandle(h);
        h = nullptr;
    }
}

template <class T> inline void SafeDelete(T*& pT)
{
    delete pT;
    pT = nullptr;
}

template <class T> inline void SafeDeleteArray(T*& pT)
{
    delete[] pT;
    pT = nullptr;
}

template <class T> inline void SafeRelease(T*& pT)
{
    if (pT != nullptr)
    {
        pT->Release();
        pT = nullptr;
    }
}

template <class T> inline void SafeFree(T*& pT)
{
    if (pT != nullptr)
    {
        free(pT);
        pT = nullptr;
    }
}

template <class T> inline void SafeCoTaskMemFree(T*& pT)
{
    if (pT != nullptr)
    {
        CoTaskMemFree(pT);
        pT = nullptr;
    }
}

#endif //__DX_COMMON_H__