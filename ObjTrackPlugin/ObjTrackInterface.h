#pragma once

#ifndef __OBJTRACK_INTERFACE_H__
#define __OBJTRACK_INTERFACE_H__

#include <Windows.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef OBJTRACKPLUGIN_EXPORTS
#define OBJTRACK_INTERFACE_API __declspec(dllexport) 
#else
#define OBJTRACK_INTERFACE_API __declspec(dllimport) 
#endif

    namespace ObjTrackInterface
    {
        OBJTRACK_INTERFACE_API HRESULT StartObjTrack(HANDLE hSharedTex);
        OBJTRACK_INTERFACE_API VOID StopObjTrack();
    }

#ifdef __cplusplus
}
#endif

#endif //__OBJTRACK_INTERFACE_H__
