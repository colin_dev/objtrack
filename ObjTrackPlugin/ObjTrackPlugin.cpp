// ObjTrackPlugin.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <Kinect.h>
#include <Windows.h>

#include "opencv2/opencv.hpp"
#include "ObjTrackInterface.h"
#include "inc/DxCommon.h"

#define VISUAL_DEBUG 0
typedef struct _THREAD_DATA
{
    HANDLE hThread;
    DWORD tId;
    HANDLE evExit;
    BOOL bKinectIR;
    cv::VideoCapture *pCam;
    ICoordinateMapper *pCoord;
    IUnknown *pFrameReader;
    int width;
    int height;
    unsigned int BPP;
    unsigned int lip;
    float dfov;
    float hfov;
    float vfov;
    HANDLE hSharedTex;
}THREAD_DATA, *PTHREAD_DATA;

#define FILTER_RELIABLE_DEPTH_VALUE(val, min, max) (((val > min) && (val < max)) ? val : 0)
#define FILTER_MIN_DEPTH_VALUE(val, min) ((val > min) ? val : min)
#define FILTER_MAX_DEPTH_VALUE(val, max) ((val < max) ? val : max)

THREAD_DATA tData = { 0 };
IKinectSensor *pKinectSensor = NULL;
ICoordinateMapper *pCoordMapper = NULL;
IInfraredFrameSource *pIRFrameSource = NULL;
IFrameDescription* pFrameDescription = NULL;
IInfraredFrameReader *pIRFrameReader = NULL;

DWORD WINAPI CamThread(_In_ PVOID pParam);

OBJTRACK_INTERFACE_API HRESULT ObjTrackInterface::StartObjTrack(HANDLE hSharedTex)
{
    HRESULT hr = S_OK;
    IInfraredFrame *pIRFrame = NULL;
    int width = 0;
    int height = 0;
    unsigned int BPP = 0;
    unsigned int lip = 0;
    float dfov = 0.0f;
    float hfov = 0.0f;
    float vfov = 0.0f;

    tData.hSharedTex = hSharedTex;

    if (SUCCEEDED(hr))
    {
        hr = ::GetDefaultKinectSensor(&pKinectSensor);
    }
    if (SUCCEEDED(hr))
    {
        hr = pKinectSensor->Open();
    }
    if (SUCCEEDED(hr))
    {
        hr = pKinectSensor->get_CoordinateMapper(&pCoordMapper);
    }
    if (SUCCEEDED(hr))
    {
        hr = pKinectSensor->get_InfraredFrameSource(&pIRFrameSource);
    }
    if (SUCCEEDED(hr))
    {
        hr = pIRFrameSource->get_FrameDescription(&pFrameDescription);
    }
    if (SUCCEEDED(hr))
    {
        hr = pIRFrameSource->OpenReader(&pIRFrameReader);
    }
    if (SUCCEEDED(hr))
    {
        pFrameDescription->get_Width(&width);
        pFrameDescription->get_Height(&height);
        pFrameDescription->get_BytesPerPixel(&BPP);
        pFrameDescription->get_LengthInPixels(&lip);
        pFrameDescription->get_DiagonalFieldOfView(&dfov);
        pFrameDescription->get_HorizontalFieldOfView(&hfov);
        pFrameDescription->get_VerticalFieldOfView(&vfov);

        // Try reading frame to check connectivity
        UINT retry = 300;
        do
        {
            hr = pIRFrameReader->AcquireLatestFrame(&pIRFrame);
            SafeRelease(pIRFrame);
            Sleep(16);
        } while (!SUCCEEDED(hr) && retry--);
    }

    if (SUCCEEDED(hr))
    {
        tData.bKinectIR = TRUE;
        tData.pCoord = pCoordMapper;
        tData.pFrameReader = pIRFrameReader;
        tData.width = width;
        tData.height = height;
        tData.BPP = BPP;
        tData.lip = lip;
        tData.dfov = dfov;
        tData.hfov = hfov;
        tData.vfov = vfov;
    }
    else
    {
        tData.pCam = new cv::VideoCapture(0);
        if (!tData.pCam->isOpened())
        {
            ::MessageBox(NULL, L"No camera detected", L"Error", MB_OK);
            ExitProcess(0);
        }
        else
        {
            tData.width = (int)tData.pCam->get(cv::CAP_PROP_FRAME_WIDTH);
            tData.height = (int)tData.pCam->get(cv::CAP_PROP_FRAME_HEIGHT);
        }
    }

    tData.evExit = ::CreateEvent(NULL, TRUE, FALSE, NULL);
    ::ResetEvent(tData.evExit);

    tData.hThread = ::CreateThread(NULL, 0, CamThread, &tData, 0, &tData.tId);

    if (tData.hThread)
    {
        hr = S_OK;
    }
    else
    {
        if (tData.pCam)
            delete tData.pCam;
        SafeRelease(pIRFrameReader);
        SafeRelease(pFrameDescription);
        SafeRelease(pIRFrameSource);
        SafeRelease(pCoordMapper);
        if (pKinectSensor)
            pKinectSensor->Close();
        SafeRelease(pKinectSensor);
    }

    return hr;
}

OBJTRACK_INTERFACE_API VOID ObjTrackInterface::StopObjTrack()
{
    ::SetEvent(tData.evExit);
    ::WaitForSingleObjectEx(tData.hThread, INFINITE, FALSE);

    if (tData.pCam)
        delete tData.pCam;
    SafeRelease(pIRFrameReader);
    SafeRelease(pFrameDescription);
    SafeRelease(pIRFrameSource);
    SafeRelease(pCoordMapper);
    if (pKinectSensor)
        pKinectSensor->Close();
    SafeRelease(pKinectSensor);
}

static void drawOptFlowMap(const cv::Mat& flow, cv::Mat& cflowmap, int step,
    double, const cv::Scalar& color)
{
    for (int y = 0; y < cflowmap.rows; y += step)
        for (int x = 0; x < cflowmap.cols; x += step)
        {
            const cv::Point2f& fxy = flow.at<cv::Point2f>(y, x);
            line(cflowmap, cv::Point(x, y), cv::Point(cvRound(x + fxy.x), cvRound(y + fxy.y)),
                color);
            circle(cflowmap, cv::Point(x, y), 2, color, -1);
        }
}

void cbTrackBar(int pos, void* userdata)
{
    uchar* pLut = (uchar*)userdata;
    float gamma = pos / 100.0f;
    // Gamma Correction
    for (int i = 0; i < 256; ++i)
        pLut[i] = cv::saturate_cast<uchar>(pow(i / 255.0, gamma) * 255.0);
}

DWORD WINAPI CamThread(_In_ PVOID pParam)
{
    PTHREAD_DATA pTData = (PTHREAD_DATA)pParam;

    HRESULT hr = E_INVALIDARG;

    if (pTData)
    {
        hr = S_OK;
    }

    ID3D11Device *pDx11Dev = NULL;
    ID3D11DeviceContext *pDx11DevCtx = NULL;
    D3D_DRIVER_TYPE driverType = D3D_DRIVER_TYPE_NULL;
    D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_11_1;
    D3D11_TEXTURE2D_DESC texDesc = { 0 };
    ID3D11Texture2D *pSharedTex = NULL;
    ID3D11Texture2D *pTex = NULL;
    D3D11_MAPPED_SUBRESOURCE mappedResource = { 0 };
    UINT resIdx = 0;
    UINT mipSlice = 0;
    UINT arraySlice = 0;
    UINT idx = 0;
    BYTE* pBits = NULL;
    DWORD width = 0;
    DWORD height = 0;
    DWORD pitch = 0;
    UINT createDeviceFlags = 0;
#ifdef _DEBUG
    createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

    D3D_DRIVER_TYPE driverTypes[] =
    {
        D3D_DRIVER_TYPE_HARDWARE,
        D3D_DRIVER_TYPE_WARP,
        D3D_DRIVER_TYPE_REFERENCE,
    };
    UINT numDriverTypes = ARRAYSIZE(driverTypes);

    D3D_FEATURE_LEVEL featureLevels[] =
    {
        D3D_FEATURE_LEVEL_11_1,
        D3D_FEATURE_LEVEL_11_0,
    };
    UINT numFeatureLevels = ARRAYSIZE(featureLevels);

    for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
    {
        driverType = driverTypes[driverTypeIndex];
        hr = D3D11CreateDevice(NULL, driverType, NULL, createDeviceFlags, featureLevels, numFeatureLevels,
            D3D11_SDK_VERSION, &pDx11Dev, &featureLevel, &pDx11DevCtx);
        if (SUCCEEDED(hr))
            break;
    }

    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->OpenSharedResource(pTData->hSharedTex, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&pSharedTex));
    }

    if (SUCCEEDED(hr))
    {
        pSharedTex->GetDesc(&texDesc);
        if ((texDesc.Format == DXGI_FORMAT_B8G8R8A8_UNORM) || (texDesc.Format == DXGI_FORMAT_R8G8B8A8_UNORM))
        {
            texDesc.Usage = D3D11_USAGE_DYNAMIC;
            texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
            texDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
            texDesc.MiscFlags = 0;
            mipSlice = resIdx % texDesc.MipLevels;
            arraySlice = resIdx / texDesc.MipLevels;
            idx = D3D11CalcSubresource(mipSlice, arraySlice, texDesc.MipLevels);
            hr = pDx11Dev->CreateTexture2D(&texDesc, NULL, &pTex);
        }
        else
        {
            hr = E_INVALIDARG;
        }
    }

    PUINT16 pBuf = NULL;
    ICoordinateMapper *pCoord = NULL;
    CameraSpacePoint *pSpaceList = NULL;
    IInfraredFrameReader *pIRFrameReader = NULL;
    IInfraredFrame *pIRFrame = NULL;
    BOOL bFirstFrame = TRUE;
    cv::Mat camMat, IRMat;
    cv::Mat prev;
    cv::Mat current;
    cv::Mat cflow, flow;
    cv::UMat uflow;
    cv::Mat visualizeHSV, visualizeBGR, hsv[3], flowChannel[2], mag, ang, tmp;
    cv::Mat edgeInput;

    cv::Mat lookUpTable(1, 256, CV_8U);
    uchar* pLut = lookUpTable.ptr();
    for (int i = 0; i < 256; ++i)
        pLut[i] = cv::saturate_cast<uchar>(pow(i / 255.0, 1.0f) * 255.0);

    int gammaI = 100;
    int noCountourFrameCount = 0;

    if (SUCCEEDED(hr))
    {
        if (pTData->bKinectIR)
        {
            pCoord = pTData->pCoord;
            pIRFrameReader = (IInfraredFrameReader*)pTData->pFrameReader;
            pCoord->AddRef();
            pIRFrameReader->AddRef();
            pBuf = (PUINT16)malloc(pTData->width * pTData->height * pTData->BPP);
            if (!pBuf)
                hr = E_INVALIDARG;
            pSpaceList = (CameraSpacePoint*)malloc(pTData->width * pTData->height * sizeof(CameraSpacePoint));
            if (!pSpaceList)
                hr = E_INVALIDARG;
        }
    }
    if (SUCCEEDED(hr))
    {
        IRMat = cv::Mat(pTData->height, pTData->width, CV_8UC1);
        if (IRMat.elemSize() == 0)
            hr = E_INVALIDARG;
    }
    if (SUCCEEDED(hr))
    {
        prev = cv::Mat(pTData->height, pTData->width, CV_8UC1);
        if (prev.elemSize() == 0)
            hr = E_INVALIDARG;
    }
    if (SUCCEEDED(hr))
    {
        current = cv::Mat(pTData->height, pTData->width, CV_8UC1);
        if (current.elemSize() == 0)
            hr = E_INVALIDARG;
    }
    if (SUCCEEDED(hr))
    {
        hsv[0] = cv::Mat::zeros(pTData->height, pTData->width, CV_8UC1);
        hsv[1] = 255 * cv::Mat::ones(pTData->height, pTData->width, CV_8UC1);
        hsv[2] = cv::Mat::zeros(pTData->height, pTData->width, CV_8UC1);
    }

#if (VISUAL_DEBUG == 1)
    if (SUCCEEDED(hr))
    {
        cv::namedWindow("IR Original", cv::WINDOW_NORMAL);
        cv::createTrackbar("Gamma", "IR Original", &gammaI, gammaI, cbTrackBar, pLut);

        cv::namedWindow("IR Opt_flow", cv::WINDOW_NORMAL);
        cv::namedWindow("IR Opt_flow_HSV", cv::WINDOW_NORMAL);
        cv::namedWindow("Object", cv::WINDOW_NORMAL);
    }
#endif

    // Contour
#define ELEMENT_SIZE 20
#define CANNY_THRESH 10
    cv::Mat canny_output;
    std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::Mat binary_image;
    cv::Mat contoursImage;
    cv::Mat contoursImageColor, contoursImageRGBA[4], *pContoursImageR;
    cv::Mat de_noise;
    cv::Mat dilate_img;
    cv::Mat element;
    BOOL needResize = (pTData->width != texDesc.Width) || (pTData->height != texDesc.Height);

    for (int i = 0; i < ARRAYSIZE(contoursImageRGBA) - 1; i++)
    {
        contoursImageRGBA[i] = cv::Mat::zeros(texDesc.Height, texDesc.Width, CV_8UC1);
    }
    contoursImageRGBA[ARRAYSIZE(contoursImageRGBA) - 1] = 255 * cv::Mat::ones(texDesc.Height, texDesc.Width, CV_8UC1);

    if (texDesc.Format == DXGI_FORMAT_B8G8R8A8_UNORM)
    {
        pContoursImageR = &contoursImageRGBA[2];
    }
    else
    {
        pContoursImageR = &contoursImageRGBA[0];
    }

    while ((SUCCEEDED(hr)) && (::WaitForSingleObjectEx(pTData->evExit, 0, FALSE) == WAIT_TIMEOUT))
    {
        if (pTData->bKinectIR)
        {
            if (SUCCEEDED(pIRFrameReader->AcquireLatestFrame(&pIRFrame)))
            {
                pIRFrame->CopyFrameDataToArray(pTData->width * pTData->height, pBuf);
                SafeRelease(pIRFrame);
            }
            UINT16 level = 0;
            for (int j = 0; j < pTData->height; j++)
            {
                for (int i = 0; i < pTData->width; i++)
                {
                    level = *(pBuf + j * pTData->width + i);
                    // Convert UINT16 to gray
                    *((PUINT8)IRMat.data + j * pTData->width + i) = (UINT8)((float)(level) / 65535.0 * 255.0f);
                }
            }
        }
        else
        {
            *pTData->pCam >> camMat;
            cv::cvtColor(camMat, IRMat, cv::COLOR_BGR2GRAY);
        }


        LUT(IRMat, lookUpTable, current);
#if (VISUAL_DEBUG == 1)
        cv::imshow("IR Original", current);
#endif

        if (bFirstFrame)
        {
            bFirstFrame = FALSE;
            continue;
        }

        cv::calcOpticalFlowFarneback(prev, current, uflow, 0.5, 3, 15, 3, 5, 1.2, 0);
        cv::cvtColor(prev, cflow, cv::COLOR_GRAY2BGR);
        uflow.copyTo(flow);

        cv::split(flow, flowChannel);
        cv::cartToPolar(flowChannel[0], flowChannel[1], mag, ang);
        hsv[0] = ang * 180 / CV_PI / 2;
        hsv[0].convertTo(hsv[0], CV_8UC1);
        cv::sqrt(flowChannel[0].mul(flowChannel[0]) + flowChannel[1].mul(flowChannel[1]), tmp);
        tmp = tmp * 4;
        edgeInput = tmp / 4;
        tmp.convertTo(tmp, CV_8UC1);
        hsv[2] = cv::min(tmp, hsv[1]);
        cv::merge(hsv, 3, visualizeHSV);
        cv::cvtColor(visualizeHSV, visualizeBGR, cv::COLOR_HSV2BGR);

        drawOptFlowMap(flow, cflow, 16, 1.5, cv::Scalar(0, 255, 0));
#if (VISUAL_DEBUG == 1)
        cv::imshow("IR Opt_flow", cflow);
        cv::imshow("IR Opt_flow_HSV", visualizeBGR);
#endif

        // Contour Detection
        edgeInput.convertTo(edgeInput, CV_8UC1);
        edgeInput = cv::min(tmp, edgeInput);

        // Get binary image using Canny edge detection
        binary_image = cv::Mat::zeros(edgeInput.size(), CV_8UC1);
        Canny(edgeInput, canny_output, CANNY_THRESH, CANNY_THRESH * 2, 3);
        findContours(canny_output, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
        for (int i = 0; i< contours.size(); i++)
        {
            drawContours(binary_image, contours, i, cv::Scalar(255), 2, 8, hierarchy, 8);
        }
        contoursImage = cv::Mat::zeros(edgeInput.size(), CV_8UC1);

        if (contours.size())
        {
            // De-noise
            cv::GaussianBlur(binary_image, de_noise, cv::Size(3, 3), 0, 0);

            // Dilate
            element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(ELEMENT_SIZE, ELEMENT_SIZE));
            //cv::dilate(de_noise, dilate_img, element);
            cv::dilate(binary_image, dilate_img, element);

            // Final contour detection
            findContours(dilate_img, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

            // Find boundray box
            int index = 0;
            int max_width = 0;
            int max_height = 0;
            for (; index >= 0; index = hierarchy[index][0]) {
                // To optimize, no need to draw object contours
                //cv::drawContours(contoursImage, contours, index, cv::Scalar(255), CV_FILLED, 8, hierarchy);
                if (contours.size())
                {
                    cv::Rect rect = boundingRect(contours[index]);
                    rectangle(contoursImage, rect, cv::Scalar(255), CV_FILLED);
                }
            }
            //noCountourFrameCount = 0;
        }
        //else
        //{
        //    //contoursImage = 255 * cv::Mat::ones(edgeInput.size(), CV_8UC1);
        //    //contoursImage = cv::Mat::zeros(edgeInput.size(), CV_8UC1);
        //    /*noCountourFrameCount++;
        //    if (noCountourFrameCount > 5)
        //    {
        //    contoursImage = 255 * cv::Mat::ones(edgeInput.size(), CV_8UC1);
        //    }*/
        //}
        if (needResize)
        {
            cv::resize(contoursImage, *pContoursImageR, pContoursImageR->size());
        }
        else
        {
            *pContoursImageR = contoursImage;
        }
        cv::merge(contoursImageRGBA, ARRAYSIZE(contoursImageRGBA), contoursImageColor);
#if (VISUAL_DEBUG == 1)
        cv::imshow("Object", contoursImage);
#endif
        std::swap(prev, current);

        if (SUCCEEDED(pDx11DevCtx->Map(pTex, idx, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource)))
        {
            pBits = (PBYTE)mappedResource.pData;
            pitch = mappedResource.RowPitch;
            width = texDesc.Width >> (1 * mipSlice);
            height = texDesc.Height >> (1 * mipSlice);
            if (pitch == contoursImageColor.step)
            {
                memcpy(pBits, contoursImageColor.data, pitch * height);
            }
            else
            {
                for (unsigned int line = 0; line < height; line++)
                {
                    memcpy(pBits + line * pitch, (PBYTE)(contoursImageColor.data) + line * contoursImageColor.step, width * 4);
                }
            }
            pDx11DevCtx->Unmap(pTex, idx);
            pDx11DevCtx->CopyResource(pSharedTex, pTex);
            pDx11DevCtx->Flush();
        }

        //if (cv::waitKey(30) >= 0) break;
        cv::waitKey(1);
        ::Sleep(16);
    }
    SafeFree(pSpaceList);
    IRMat.release();
    SafeRelease(pIRFrame);
    SafeRelease(pIRFrameReader);
    SafeRelease(pCoord);
    SafeFree(pBuf);

    SafeRelease(pSharedTex);
    SafeRelease(pTex);
    SafeRelease(pDx11DevCtx);
    SafeRelease(pDx11Dev);

    return 0;
}
