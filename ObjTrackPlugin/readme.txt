简介：
ObjTrackPlugin为DLL调用库，可以获得动态目标的捕获区域，背景色为黑色，捕获到的目标区域为红色。
支持Kinect IR摄像头或普通摄像头。连接Kinect则使用Kinect摄像头，否则使用第一个枚举到的普通摄像头。

使用说明：
1. 创建shared resource。如ObjTrackTest.cpp中line 400-405。
  - ID3D11Device需是需要访问该shared resource的device。
  - 尺寸可根据需要指定。
  - 格式只能使用DXGI_FORMAT_B8G8R8A8_UNORM或DXGI_FORMAT_R8G8B8A8_UNORM。
  - 其余flag必须如例子所示。
2. 获取shared resource所对应的HANDLE。如ObjTrackTest.cpp中line 408-422。
3. 使用2中创建的HANDLE，调用接口ObjTrackInterface::StartObjTrack(HANDLE)。如ObjTrackTest.cpp中line 412。
4. 任意时刻，可以使用CopyResource()获得最新更新的内容。如ObjTrackTest.cpp中line 486。
5. 使用完毕，调用接口ObjTrackInterface::StopObjTrack()。如ObjTrackTest.cpp中line 498。
6. 其余工作，均由ObjTrackPlugin完成。
7. 需copy需要的第三方库至执行文件同一目录，否则执行会出错。包括：
  - ObjTrackPlugin.dll
  - opencv_world341d.dll或opencv_world341.dll
  - opencv_ffmpeg341_64.dll